import "./myStyles.css";
import { useState } from "react";
import spark from "./../assets/images/spark.png";
import whiteball1 from "./../assets/images/whiteball1.png";
import whiteball2 from "./../assets/images/whiteball2.png";
import whiteball3 from "./../assets/images/whiteball3.png";
import whiteball4 from "./../assets/images/whiteball4.png";
import whiteball5 from "./../assets/images/whiteball5.png";
import blackball1 from "./../assets/images/blackball1.png";
import blackball2 from "./../assets/images/blackball2.png";
import blackball3 from "./../assets/images/blackball3.png";
import blackball4 from "./../assets/images/blackball4.png";
import blackball5 from "./../assets/images/blackball5.png";

function SoccerBall() {
  const [image, setImage] = useState(false);
  const clicked = () => {
    image ? setImage(false) : setImage(true);
  };
  console.log(image);
  return (
    <div>
      <div className="container1">
        <div
          id="spark"
          style={{
            backgroundImage: `url(${spark})`,
          }}
        />
      </div>
      <div className="container2">
        <div id="label1">National Brand Premium Soccer Balls from India</div>
        <div
          id="ball1"
          style={{
            backgroundImage: image
              ? `url(${blackball1})`
              : `url(${whiteball1})`,
          }}
          onClick={clicked}
        />
        <div id="label2">
          Hand stitched, unique 8 panel design, NFHS approved
        </div>
      </div>
      <div className="container3">
        <div id="label1">Pick your favourite ball</div>
        <div
          id="ball1"
          style={{
            backgroundImage: image
              ? `url(${blackball1})`
              : `url(${whiteball1})`,
          }}
        />
        <div id="label2">
          {image
            ? "Umbro NFHS Tempest Soccer ball"
            : "Umbro NFHS Meteor Soccer ball"}
        </div>
        <div
          id="ball2"
          style={{
            backgroundImage: image
              ? `url(${blackball2})`
              : `url(${whiteball2})`,
          }}
        />
        <div
          id="ball3"
          style={{
            backgroundImage: image
              ? `url(${blackball3})`
              : `url(${whiteball3})`,
          }}
        />
        <div
          id="ball4"
          style={{
            backgroundImage: image
              ? `url(${blackball4})`
              : `url(${whiteball4})`,
          }}
        />
        <div
          id="ball5"
          style={{
            backgroundImage: image
              ? `url(${blackball5})`
              : `url(${whiteball5})`,
          }}
        />
      </div>
    </div>
  );
}

export default SoccerBall;
