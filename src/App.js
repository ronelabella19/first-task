import "./App.css";
import SoccerBall from "./components/SoccerBall";

function App() {
  return (
    <div className="App">
      <SoccerBall />
    </div>
  );
}

export default App;
